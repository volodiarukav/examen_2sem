#pragma once
#include <iostream>

class Slau
{
public:

    Slau(int N)
    {
#ifdef MY_DEBUG
        std::cout << "Constructor" << std::endl;
#endif
        m_n = N;
        m_a = new double* [m_n];
        m_y = new double[m_n];
        for (int i = 0; i < m_n; i++)
        {
            m_a[i] = new double[m_n];
            for (int j = 0; j < m_n; j++)
            {
                std::cout << "a[" << i << "][" << j << "]= ";
                std::cin >> m_a[i][j];
            }
        }
        for (int i = 0; i < m_n; i++)
        {
            std::cout << "y[" << i << "]= ";
            std::cin >> m_y[i];
        }
    }

    ~Slau()
    {
#ifdef MY_DEBUG 
        std::cout << "Destructor" << std::endl;
#endif

        //  double** m_a, * m_y, * m_x;  ?????
        sysout();
        gauss();
        delete[] m_y;
        for (int i = 0; i < m_n; i++)
            delete [] m_a[i];
        delete [] m_a;
    }

    // ����� ������� ���������
    void sysout()
    {
        for (int i = 0; i < m_n; i++)
        {
            for (int j = 0; j < m_n; j++)
            {
                std::cout << m_a[i][j] << "*x" << j;
                if (j < m_n - 1)
                    std::cout << " + ";
            }
            std::cout << " = " << m_y[i] << std::endl;
        }
        //return;
    }

    // ���������� �����
    double gauss()
    {
        
        double* x, max;
        int k, index;
        const double eps = 0.00001;  // ��������
        x = new double[m_n];
        k = 0;
        while (k < m_n)
        {
            // ����� ������ � ������������ a[i][k]
            max = abs(m_a[k][k]);
            index = k;
            for (int i = k + 1; i < m_n; i++)
            {
                if (abs(m_a[i][k]) > max)
                {
                    max = abs(m_a[i][k]);
                    index = i;
                }
            }
            // ������������ �����
            if (max < eps)
            {
                // ��� ��������� ������������ ���������
                std::cout << "������� �������� ���������� ��-�� �������� ������� ";
                std::cout << index << " ������� A" << std::endl;
                return -1;// ���������� ��������� 
                //exit(0);
            }
            for (int j = 0; j < m_n; j++)
            {
                double temp = m_a[k][j];
                m_a[k][j] = m_a[index][j];
                m_a[index][j] = temp;
            }
            double temp = m_y[k];
            m_y[k] = m_y[index];
            m_y[index] = temp;
            // ������������ ���������
            for (int i = k; i < m_n; i++)
            {
                double temp = m_a[i][k];
                if (abs(temp) < eps) continue; // ��� �������� ������������ ����������
                for (int j = 0; j < m_n; j++)
                    m_a[i][j] = m_a[i][j] / temp;
                m_y[i] = m_y[i] / temp;
                if (i == k)  continue; // ��������� �� �������� ���� �� ����
                for (int j = 0; j < m_n; j++)
                    m_a[i][j] = m_a[i][j] - m_a[k][j];
                m_y[i] = m_y[i] - m_y[k];
            }
            k++;
        }
        // �������� �����������
        for (k = m_n - 1; k >= 0; k--)
        {
            x[k] = m_y[k];
            for (int i = 0; i < k; i++)
                m_y[i] = m_y[i] - m_a[i][k] * x[k];
        }

        // ����� ������ 
        for (int i = 0; i < m_n; i++)
            std::cout << "x[" << i << "]=" << x[i] << std::endl;

        delete[] x;
    }

    

private:
    int m_n;
    double** m_a, * m_y;
};

/*
    int Result()
    {
        sysout(m_a, m_y, m_n);
        double* m_x;
        m_x = gauss(m_a, m_y, m_n);

        // ����� ������
        for (int i = 0; i < m_n; i++)
            std::cout << "x[" << i << "]=" << m_x[i] << std::endl;
        return 0;
    }*/

/*
//using namespace std;
// ����� ������� ���������
void sysout(double** a, double* y, int n)
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            std::cout << a[i][j] << "*x" << j;
            if (j < n - 1)
                std::cout << " + ";
        }
        std::cout << " = " << y[i] << std::endl;
    }
    //return;
}

// ���������� �����
double* gauss(double** a, double* y, int n)
{
    double* x, max;
    int k, index;
    const double eps = 0.00001;  // ��������
    x = new double[n];
    k = 0;
    while (k < n)
    {
        // ����� ������ � ������������ a[i][k]
        max = abs(a[k][k]);
        index = k;
        for (int i = k + 1; i < n; i++)
        {
            if (abs(a[i][k]) > max)
            {
                max = abs(a[i][k]);
                index = i;
            }
        }
        // ������������ �����
        if (max < eps)
        {
            // ��� ��������� ������������ ���������
            std::cout << "������� �������� ���������� ��-�� �������� ������� ";
            std::cout << index << " ������� A" << std::endl;
            return 0;// ���������� ��������� 
        }
        for (int j = 0; j < n; j++)
        {
            double temp = a[k][j];
            a[k][j] = a[index][j];
            a[index][j] = temp;
        }
        double temp = y[k];
        y[k] = y[index];
        y[index] = temp;
        // ������������ ���������
        for (int i = k; i < n; i++)
        {
            double temp = a[i][k];
            if (abs(temp) < eps) continue; // ��� �������� ������������ ����������
            for (int j = 0; j < n; j++)
                a[i][j] = a[i][j] / temp;
            y[i] = y[i] / temp;
            if (i == k)  continue; // ��������� �� �������� ���� �� ����
            for (int j = 0; j < n; j++)
                a[i][j] = a[i][j] - a[k][j];
            y[i] = y[i] - y[k];
        }
        k++;
    }
    // �������� �����������
    for (k = n - 1; k >= 0; k--)
    {
        x[k] = y[k];
        for (int i = 0; i < k; i++)
            y[i] = y[i] - a[i][k] * x[k];
    }
    return x;
}*/

/*
int main()
{
    double** a, * y, * x;
    int n;
    system("chcp 1251");
    system("cls");
    cout << "������� ���������� ���������: ";
    cin >> n;
    a = new double* [n];
    y = new double[n];
    for (int i = 0; i < n; i++)
    {
        a[i] = new double[n];
        for (int j = 0; j < n; j++)
        {
            cout << "a[" << i << "][" << j << "]= ";
            cin >> a[i][j];
        }
    }
    for (int i = 0; i < n; i++)
    {
        cout << "y[" << i << "]= ";
        cin >> y[i];
    }
    sysout(a, y, n);
    x = gauss(a, y, n);
    for (int i = 0; i < n; i++)
        cout << "x[" << i << "]=" << x[i] << endl;
    cin.get(); cin.get();
    return 0;
}*/